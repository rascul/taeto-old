use std::io::Read;

use hyper::Client;
use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::{IrcServer, Server};
use irc::client::server::utils::ServerExt;
use rustc_serialize::json::Json;
use tokio_core::reactor::Core;

use util;

struct Beer {
	name: String,
	brewery: String,
	url: String,
}

impl Beer {
	fn new() -> Self {
		Beer {
			name: "milk".to_string(),
			brewery: "Kemps".to_string(),
			url: "http://www.kemps.com/fridge/milk/select-vitamin-d-milk-pint".to_string(),
		}
	}
}

pub fn help() -> String {
	format!(
		"beer [{}] :: Pass out beer, optionally to {}, or {} for everyone",
		util::italic("target"),
		util::italic("target"),
		util::italic("round")
	).to_string()
}

fn get_beer(json: Json) -> Beer {
	// this nest of ifs is silly but i don't know a better way
	
	let mut beer = Beer::new();
	
	if let Some(value) = json.find_path(&["data", "name"]) {
		if let Some(name) = value.as_string() {
			beer.name = name.to_string();
		} else {
			return Beer::new();
		}
	} else {
		return Beer::new();
	}
	
	if let Some(value) = json.find_path(&["data", "id"]) {
		if let Some(url) = value.as_string() {
			beer.url = format!("http://www.brewerydb.com/beer/{}", url);
		} else {
			return Beer::new();
		}
	} else {
		return Beer::new();
	}
	
	if let Some(value) = json.find_path(&["data", "breweries"]) {
		if let Some(array) = value.as_array() {
			if array.len() > 0 {
				if let Some(inner_value) = array[0].search("name") {
					if let Some(brewery) = inner_value.as_string() {
						beer.brewery = brewery.to_string();
					} else {
						return Beer::new();
					}
				} else {
					return Beer::new();
				}
			} else {
				return Beer::new();
			}
		} else {
			return Beer::new();
		}
	} else {
		return Beer::new();
	}	
	
	beer
}

pub fn fetch_url(url: String) {
	let mut core = Core::new()?;
	let client = Client::new(&core.handle());
	let work = client.get(url.parse()?)

pub fn command(server: IrcServer, message: Message, target: &String, msg: &String) {
	let apikey = server.config().get_option("beer_apikey");
	let client = Client::new();
	let mut beer = Beer::new();
	
	if let Ok(mut res) = client.get(
		&format!("https://api.brewerydb.com/v2/beer/random/?key={}&withBreweries=y", apikey)
	).send() {
		let mut buf = String::new();
		res.read_to_string(&mut buf).unwrap();
		
		if let Ok(json) = Json::from_str(&buf) {
			beer = get_beer(json);
		}
	}
	
	let args = util::parse_args(msg.clone());
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else if args[1] == "round" {
			"round".to_string()
	} else {
		args[1].to_string()
	};
	
	if user == "round" {
		server.send_privmsg(target, &util::action(
			&format!("passes out a round of {} from {}. {}", beer.name, beer.brewery, beer.url)
		)).unwrap();
	} else {
		server.send_privmsg(target, &util::action(
			&format!("hands {} a {} from {}. {}", user, beer.name, beer.brewery, beer.url)
		)).unwrap();
	}
}
