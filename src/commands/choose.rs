use irc::proto::Message;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use rand::{Rng, thread_rng};

use util;

pub fn help() -> String {
	format!(
		"choose {} ... :: Choose between the given choices", util::italic("choice")
	).to_string()
}

pub fn command(server: IrcServer, _: Message, target: &String, msg: &String) {
	let mut args = util::parse_args(msg.clone());
	
	if args.len() > 1 {
		args.remove(0);
		let ref choice = args[thread_rng().gen_range(0, args.len())];
		server.send_privmsg(target, &format!("{}", choice)).unwrap();
	}
}
