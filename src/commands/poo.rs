use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use util;

pub fn help() -> String {
	format!(
		"poo [{}] :: Throw poo, optionally at {}",
		util::italic("target"),
		util::italic("target")
	).to_string()
}

pub fn command(server: IrcServer, message: Message, target: &String, msg: &String) {
	let args = util::parse_args(msg.clone());
	
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else {
		args[1].to_string()
	};
	
	server.send_privmsg(
		target,
		&util::action(&format!("throws poo at {}", user))
	).unwrap();
}
