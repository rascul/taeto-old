//mod beer;
mod buffalo;
mod choose;
mod cookie;
mod magic8ball;
mod poo;
mod sammich;

use irc::proto::Message;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use ::channels::Channel;

lazy_static! {
	static ref REGISTER: Vec<(
		&'static str, fn() -> String, fn(IrcServer, Message, &String, &String)
	)> = vec![
		// name        help function        command function
		("8",          magic8ball::help,    magic8ball::command),
//		("beer",       beer::help,          beer::command),
		("buffalo",    buffalo::help,       buffalo::command),
		("choose",     choose::help,        choose::command),
		("cookie",     cookie::help,        cookie::command),
		("poo",        poo::help,           poo::command),
		("sammich",    sammich::help,       sammich::command),
	];
}

fn search_register(key: String) -> Option<(
	&'static str, fn() -> String, fn(IrcServer, Message, &String, &String)
)> {
	for item in REGISTER.iter() {
		if key == item.0 {
			return Some(*item);
		}
	}
	None
}

pub fn process(
	server: IrcServer, channel: Channel, message: Message, target: &String, msg: &String
) {
	let mut command = msg.split_whitespace().next().unwrap().to_string();
	
	for _ in 0..channel.prefix.len() {
		command.remove(0);
	}
	
	if command == "help" {
		help(server, channel, target, msg);
	} else {
		if channel.commands.contains(&command) {
			if let Some(entry) = search_register(command) {
				entry.2(server, message, target, msg);
			}
		}
	}
}

fn help(server: IrcServer, channel: Channel, target: &String, msg: &String) {
	let args: Vec<&str> = msg.split_whitespace().collect();

	if args.len() > 1 {
		let help_command = args[1];
		if channel.commands.contains(&help_command.to_string()) {
			if let Some(entry) = search_register(help_command.to_string()) {
				server.send_privmsg(target, &entry.1()).unwrap();
			}
		}
	} else {
		let mut commands: Vec<&str> = Vec::new();

		for item in channel.commands.clone() {
			if let Some(entry) = search_register(item) {
				commands.push(entry.0);
			}
		}

		if commands.len() > 0 {
			commands.sort();
			server.send_privmsg(
				target, &format!("Available commands: {}", commands.join(" "))
			).unwrap();
		}
	}
}
