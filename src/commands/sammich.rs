use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use rand::{Rng, thread_rng};

use util;

lazy_static! {
	static ref SAMMICHES: Vec<&'static str> = vec![
		"barbecue beef",
		"barbecue chicken",
		"barbecue pork",
		"beef",
		"blt",
		"bologna",
		"cheesesteak",
		"chicken",
		"chicken salad",
		"club",
		"deli",
		"french dip",
		"grilled cheese",
		"grilled cheese and ham",
		"ham",
		"ham and cheese",
		"ice cream",
		"lettuce",
		"meatball",
		"peanut butter",
		"peanut butter and jelly",
		"pulled beef",
		"pulled chicken",
		"pulled pork",
		"reuben",
		"roast beef",
		"roast turkey",
		"sloppy joe",
		"smoked chicken",
		"smoked turkey",
		"steak",
		"submarine",
		"tuna",
		"turkey",
	];
}

pub fn help() -> String {
	format!(
		"sammich [{}] :: Give a sammich, optionally to {}",
		util::italic("target"),
		util::italic("target")
	).to_string()
}

pub fn command(server: IrcServer, message: Message, target: &String, msg: &String) {
	let sammich = SAMMICHES[thread_rng().gen_range(0, SAMMICHES.len())];
	let args = util::parse_args(msg.clone());
	
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else {
		args[1].to_string()
	};
	
	server.send_privmsg(
		target, &util::action(&format!("gives {} a {} sammich.", user, sammich))
	).unwrap();
}
