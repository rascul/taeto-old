use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use rand::{Rng, thread_rng};

use util;

lazy_static! {
	static ref BUFFALOS: Vec<&'static str> = vec![
		"a decrepit",
		"a delicious",
		"a fat",
		"a healthy",
		"a large",
		"an old",
		"an oversized",
		"a sickly",
		"a skinny",
		"a small",
		"a smelly",
		"a young",
		"a wildly winged",
		"a smoked",
		"a cooked",
	];
}

pub fn help() -> String {
	format!(
		"buffalo [{}] :: Throw a buffalo, optionally at {}",
		util::italic("target"),
		util::italic("target")
	).to_string()
}

pub fn command(server: IrcServer, message: Message, target: &String, msg: &String) {
	let buffalo = BUFFALOS[thread_rng().gen_range(0, BUFFALOS.len())];
	let args = util::parse_args(msg.clone());
	
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else {
		args[1].to_string()
	};
	
	server.send_privmsg(
		target,
		&util::action(&format!("throws {} buffalo at {}", buffalo, user))
	).unwrap();
}
