use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use util;

pub fn help_say() -> String {
	format!(
		"say {} {} :: say {} in {}",
		util::italic("channel"),
		util::italic("message"),
		util::italic("message"),
		util::italic("channel")
	).to_string()
}

pub fn help_emote() -> String {
	format!(
		"do {} {} :: do {} in {}",
		util::italic("channel"),
		util::italic("message"),
		util::italic("message"),
		util::italic("channel")
	).to_string()
}

pub fn command_say(server: IrcServer, message: Message, target: &String, msg: &String) {
	let args: Vec<&str> = msg.split_whitespace().collect();
	
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else {
		args[1].to_string()
	};
	
	server.send_privmsg(
		target,
		&util::action(&format!("gives {} a {} sammich.", user, sammich))
	).unwrap();
}
