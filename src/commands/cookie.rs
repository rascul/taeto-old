use irc::proto::Message;
use irc::client::data::user::User;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use rand::{Rng, thread_rng};

use util;

lazy_static! {
	static ref COOKIES: Vec<&'static str> = vec![
		"a chocolate chip cookie",
		"a butter cookie",
		"a sugar cookie",
		"a peanut butter cookie",
		"an oatmeal raisin cookie",
		"an oreo",
		"a hamantash",
		"a snickerdoodle",
		"a spritzgebäck",
		"a fortune cookie",
		"an alfajor",
		"a berger cookie",
		"a berner haselnusslebkuchen",
		"a berner honiglebkuchen",
		"a biscotti",
		"a ginger snap",
		"a gingerbread cookie",
		"a springerle",
		"a sandbakelse",
		"a pfeffernüsse",
		"a qurabiya",
		"a rainbow cookie",
		"a rosette",
		"a sablé",
		"a shortbread cookie",
		"a speculaas",
		"a thin mint",
		"a do-si-do",
		"a caramel delite",
		"a tagalong",
	];
}

pub fn help() -> String {
	format!(
		"cookie [{}] :: Give a cookie, optionally to {}",
		util::italic("target"),
		util::italic("target")
	).to_string()
}

pub fn command(server: IrcServer, message: Message, target: &String, msg: &String) {
	let cookie = COOKIES[thread_rng().gen_range(0, COOKIES.len())];
	let args = util::parse_args(msg.clone());
	
	let user = if args.len() == 1 {
		let u = User::new(message.prefix.unwrap().as_str());
		u.get_nickname().to_string()
	} else {
		args[1].to_string()
	};
	
	server.send_privmsg(
		target, &util::action(&format!("gives {} {}.", user, cookie))
	).unwrap();
}
