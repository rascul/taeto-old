use irc::proto::Message;
use irc::client::server::IrcServer;
use irc::client::server::utils::ServerExt;

use rand::{Rng, thread_rng};

use util;

lazy_static! {
	static ref RESPONSES: Vec<&'static str> = vec![
		"As I see it, yes.",
		"As I see it, no.",
		"Ask again later.",
		"Better not tell you now.",
		"Cannot predict now.",
		"Concentrate and ask again.",
		"Count on it.",
		"Don't count on it.",
		"False.",
		"It's certain.",
		"It's not certain.",
		"It is decidedly so.",
		"It's a secret.",
		"Most likely.",
		"My reply is no.",
		"My reply is yes.",
		"Outlook good.",
		"Outlook not so good.",
		"Reply hazy, try again.",
		"Signs point to no.",
		"Signs point to yes.",
		"That's a definite possibility.",
		"True.",
		"Very doubtful.",
		"Without a doubt.",
		"Yes, definitely.",
		"You may rely on it.",
		"Yes."
	];
}

pub fn help() -> String {
	format!(
		"8 [{}] :: Make a prediction for {}",
		util::italic("question"),
		util::italic("question")
	).to_string()
}

pub fn command(server: IrcServer, _: Message, target: &String, _: &String) {
	let response = RESPONSES[thread_rng().gen_range(0, RESPONSES.len())];
	server.send_privmsg(target, &util::action(&format!("{}", response))).unwrap();
}
