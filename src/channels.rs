use std::collections::HashMap;
use std::fs::File;
use std::io::{ErrorKind, BufReader, Read};
use std::io::Error as IoError;

use rustc_serialize::json::{decode, encode, Json};

use error::TaetoResult;

#[derive(Clone, RustcDecodable)]
pub struct Channel {
	pub prefix: String,
	pub commands: Vec<String>,
}

pub fn load(path: &str) -> TaetoResult<HashMap<String, Channel>> {
	let file = try!(File::open(path));
	let mut buf = BufReader::new(file);
	let mut content = String::new();
	
	try!(buf.read_to_string(&mut content));
	
	let json = try!(Json::from_str(&content));
	let object = try!(json.as_object().ok_or(IoError::new(
		ErrorKind::NotFound, "Channels object not found"
	)));
	
	let mut channels: HashMap<String, Channel> = HashMap::new();
	for (key, value) in object.iter() {
		let channel: Channel = try!(decode(&try!(encode(value))));
		channels.insert(key.to_owned(), channel);
	}
	
	Ok(channels)
}
