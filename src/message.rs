use std::collections::HashMap;

use irc::proto::Command;
use irc::proto::Message;
use irc::client::server::IrcServer;

use channels::Channel;
use commands;

pub fn process(server: IrcServer, channels: HashMap<String, Channel>, message: Message) {
	match message.command {
		Command::PRIVMSG(ref target, ref msg) => {
			if let Some(channel) = channels.get(target) {
				if msg.starts_with(&channel.prefix) {
					commands::process(server, channel.clone(), message.clone(), target, msg);
				}
			}
		},
		_ => (),
	}
}
