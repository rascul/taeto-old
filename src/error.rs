use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::Error as IoError;

use irc::error::Error as IrcError;
use rustc_serialize::json::{DecoderError, EncoderError, ParserError};

pub type TaetoResult<T> = Result<T, TaetoError>;

#[derive(Debug)]
pub enum TaetoError {
	Decoder(DecoderError),
	Encoder(EncoderError),
	Io(IoError),
	Irc(IrcError),
	Parser(ParserError),
}

impl From<DecoderError> for TaetoError {
	fn from(e: DecoderError) -> TaetoError {
		TaetoError::Decoder(e)
	}
}

impl From<EncoderError> for TaetoError {
	fn from(e: EncoderError) -> TaetoError {
		TaetoError::Encoder(e)
	}
}

impl From<IoError> for TaetoError {
	fn from(e: IoError) -> TaetoError {
		TaetoError::Io(e)
	}
}

impl From<IrcError> for TaetoError {
	fn from(e: IrcError) -> TaetoError {
		TaetoError::Irc(e)
	}
}

impl From<ParserError> for TaetoError {
	fn from(e: ParserError) -> TaetoError {
		TaetoError::Parser(e)
	}
}

impl Display for TaetoError {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match *self {
			TaetoError::Decoder(ref e) => Display::fmt(e, f),
			TaetoError::Encoder(ref e) => Display::fmt(e, f),
			TaetoError::Io(ref e) => Display::fmt(e, f),
			TaetoError::Irc(ref e) => Display::fmt(e, f),
			TaetoError::Parser(ref e) => Display::fmt(e, f),
		}
	}
}
