pub fn action(text: &str) -> String {
	/*let mut s = "\x01ACTION ".to_string();
	s.push_str(text);
	s.push_str("\x01");
	s*/
	format!("\x01ACTION {}\x01", text)
}

pub fn italic(text: &str) -> String {
	format!("\x1D{}\x1D", text)
}

pub fn parse_args(string: String) -> Vec<String> {
	let mut split: Vec<String> = Vec::new();
	let mut quote_start = false;
	let mut buf = String::new();

	for slice in string.split_whitespace() {
		let mut slice = slice.to_string();
		if !quote_start {
			if slice.starts_with('"') {
				slice.remove(0);
				buf = slice.to_string();
				quote_start = true;
			} else {
				split.push(slice.to_string());
			}
		} else {
			buf.push_str(" ");
			buf.push_str(&slice);

			if slice.ends_with('"') {
				buf.pop().unwrap();
				split.push(buf);
				buf = String::new();
				quote_start = false;
			}
		}
	}

	split
}

