extern crate futures;
extern crate hyper;
extern crate irc;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate rag;
extern crate rand;
extern crate rustc_serialize;
extern crate simplelog;
extern crate tokio_core;

mod channels;
mod commands;
mod error;
mod message;
mod util;

use std::process::exit;
use std::thread::spawn;

use irc::client::data::config::Config;
use irc::client::server::{IrcServer, Server};
use irc::client::server::utils::ServerExt;

//use simplelog::*;

fn run_server() -> error::TaetoResult<()> {
	let config = Config::load("config.json")?;
	let channels = channels::load("channels.json")?;
	let server = IrcServer::from_config(config)?;

	server.identify()?;
	server.for_each_incoming(|message| {
		//let message = message.unwrap();
		let server = server.clone();
		let channels = channels.clone();
		info!("{}", message);
		let _ = spawn(move || message::process(server, channels, message));
	});

	Ok(())
}

fn main() {
	//CombinedLogger::init(vec![
	//	TermLogger::new(LogLevelFilter::Info, simplelog::Config::default()).unwrap(),
	//]).unwrap();
	rag::init();

	if let Err(e) = run_server() {
		error!("Error: {}", e);
		exit(1);
	}
}
